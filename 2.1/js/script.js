function swap_img() {
        let first_img_obj = document.getElementById("first");
        let second_img_obg = document.getElementById("second");

        let first_img = first_img_obj.getAttribute("src");
        let second_img = second_img_obg.getAttribute("src");

        first_img_obj.setAttribute("src",second_img);
        second_img_obg.setAttribute("src",first_img);
}