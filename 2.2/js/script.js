let interval = 500;
let time;
let i = 1;

function setPinterwal() {
    document.getElementById("interval-P").innerHTML = interval;
}

function st_sp() {
    setPinterwal();
    let status = document.getElementById("status").textContent;
    if (status === "start") {
        document.getElementById("status").innerHTML = "stop";
        document.getElementById("up").disabled = false;
        document.getElementById("down").disabled = false;
        start();
    } else {
        document.getElementById("status").innerHTML = "start";
        document.getElementById("up").disabled = true;
        document.getElementById("down").disabled = true;
        stop();
    }
}

function speed_up() {
    interval -= 100;
    if (interval < 0) {
        interval = 0;
    }
    setPinterwal();
    stop();
    start();
}

function speed_down() {
    interval += 100;
    setPinterwal();
    stop();
    start();
}

function stop() {
    clearInterval(time);
}

async function start() {
    time = setInterval(function run() {
        document.getElementById("gifs").setAttribute("src", "src/s" + i + ".gif");
        if (i === 12) {
            i = 0;
        }
        i++;
    }, interval);
}