let curs = new XMLHttpRequest();
curs.open("GET", "xml/curs.xml", false);
curs.send();

let groups = new XMLHttpRequest();
groups.open("GET", "xml/groups.xml", false);
groups.send();

let marks = new XMLHttpRequest();
marks.open("GET", "xml/marks.xml", false);
marks.send();

let cursXml = curs.responseXML,
    groupsXml = groups.responseXML,
    marksXml = marks.responseXML;


let groupsList = [],
    facultyList = [],
    items = [];

let currentGroup,
    currentFaculty,
    currentCourse;

marksXml.childNodes[0].childNodes.forEach(function (faculty) {
    if (typeof(faculty) !== "undefined" && faculty.childNodes.length !== 0) {
        let option = document.createElement("option");
        option.setAttribute("value", faculty.attributes[1].value);
        option.innerText = faculty.attributes[0].value;
        document.getElementById("faculty").appendChild(option);
    }
});

cursXml.childNodes[0].childNodes.forEach(function (course) {
    if (typeof(course) !== "undefined" && course.childNodes.length !== 0) {
        let option = document.createElement("option");
        option.setAttribute("value", course.attributes[0].value);
        option.innerText = course.attributes[0].value;
        document.getElementById("course").appendChild(option);
    }
});

cursXml.childNodes[0].childNodes.forEach(function (node) {
    let sch = [];
    if (node.nodeName !== "#text") {
        node.childNodes.forEach(function (item) {
            if (item.nodeName !== "#text") {
                sch.push({
                    code: item.getAttribute("code"),
                    item: item.textContent
                });
            }
        });
        items.push({
            course: node.getAttribute("value"),
            curs: sch
        });

    }
});

groupsXml.childNodes[0].childNodes.forEach(function (group) {
    if (typeof(group) !== "undefined" && group.childNodes.length !== 0) {
        let students = [];
        group.childNodes.item(3).childNodes.forEach(function (e) {
            if (e.childNodes.length) {
                students.push({
                    code: e.getAttribute("code"),
                    name: e.textContent
                });
            }
        });
        groupsList.push({
            groupCode: group.getAttribute("code"),
            facultyCode: group.getAttribute("facultyCode"),
            course: group.getAttribute("course"),
            name: group.childNodes.item(1).textContent,
            students: students,
        });
    }
});

marksXml.childNodes.forEach(function (node) {
    let facultyArray = [];
    node.childNodes.forEach(function (faculty) {
        let facultyCode;
        if (faculty.nodeName !== "#text") {
            let groupArray = [];
            facultyCode = faculty.getAttribute("facultyCode");
            faculty.childNodes.forEach(function (group) {
                let groupCode;
                if (group.nodeName !== "#text") {
                    let studentArray = [];
                    groupCode = group.getAttribute("code");
                    group.childNodes.forEach(function (mark) {
                        let studentCode;
                        if (mark.nodeName !== "#text") {
                            let markArray = [];
                            studentCode = mark.getAttribute("studentCode");
                            mark.childNodes.forEach(function (item) {
                                if (item.nodeName !== "#text") {
                                    let itemCode = item.childNodes[1].textContent;
                                    let mark = item.childNodes[3].textContent;
                                    markArray.push({
                                        item: itemCode,
                                        mark: mark
                                    });
                                }
                            });
                            studentArray.push({
                                code: studentCode,
                                marks: markArray
                            });
                        }
                    });
                    groupArray.push({
                        code: groupCode,
                        students: studentArray
                    });
                }
            });
            facultyArray.push({
                code: facultyCode,
                groups: groupArray
            });
        }
    });
    facultyList = facultyArray;
});

document.getElementById("group").addEventListener("change", function () {
    let selected = this.value;
    let requested = groupsList.find(function (e) {
        return e.groupCode === selected;
    });
    let target = document.getElementById("name");
    target.innerHTML = "";
    requested.students.forEach(function (e) {
        let option = document.createElement("option");
        option.setAttribute("value", e.code);
        option.innerText = e.name;
        target.appendChild(option);
    });
    currentGroup = selected;
});

document.getElementById("faculty").addEventListener("change", function () {
    currentFaculty = this.value;
    document.getElementById("course").dispatchEvent(new Event("change"));
});

document.getElementById("course").addEventListener("change", function () {
    let selected = this.value;
    currentCourse = selected;
    document.getElementById("group").innerHTML = "";
    groupsList.forEach(function (e) {
        if (e.course === selected && e.facultyCode === currentFaculty) {
            let option = document.createElement("option");
            option.setAttribute("value", e.groupCode);
            option.innerText = e.name;
            let target = document.getElementById("group");
            target.appendChild(option);
        }
    });
    document.getElementById("group").dispatchEvent(new Event("change"));
    document.getElementById("name").dispatchEvent(new Event("change"));
});

document.getElementById("name").addEventListener("change", function () {

    let selected = this.value;
    let target = document.getElementById("target");
    target.innerHTML = "";
    let marks = facultyList.find(e => e.code === currentFaculty)
        .groups.find(e => e.code === currentGroup)
        .students.find(e => e.code === selected)
        .marks;
    let curs = items.find(e => e.course === currentCourse).curs;
    curs.forEach(function (e) {
        let itemCode = e.code;
        let itemName = e.item;
        let currentMark = marks.find(value => value.item === itemCode).mark;
        let tr = document.createElement("tr");
        let name = document.createElement("td");
        let mrk = [];
        for (let i = 0; i < 3; i++) {
            mrk.push(document.createElement("input"));
            mrk[i].value = i;
            mrk[i].type = "radio";
            mrk[i].name = "item-" + itemCode;
        }
        name.innerHTML = itemName;
        tr.appendChild(name);
        mrk.forEach(function (mark) {
            let td = document.createElement("td");
            if (mark.value === currentMark) {
                mark.setAttribute("checked", "checked");
            }
            td.appendChild(mark);
            tr.appendChild(td);
        });
        target.appendChild(tr);
    });
});

document.getElementById("calculate").addEventListener("click", function () {
    let itemsCount = items.find(val => val.course === currentCourse).curs.length;
    let scoreSum = 0;
    items.forEach(function (e) {
        if (currentCourse === e.course) {
            e.curs.forEach(function (e) {
                let value = document.querySelector("input[name=item-" + e.code + "]:checked").value;
                value = parseFloat(value);
                scoreSum += value;
            });
        }
    });
    let result = scoreSum / itemsCount;
    document.getElementById("score").innerText = result.toFixed(2);
});

document.getElementById("clear").addEventListener("click", function () {
    document.getElementById("score").innerText = "NONE";
});



document.getElementById("faculty").dispatchEvent(new Event("change"));
document.getElementById("calculate").dispatchEvent(new Event("click"));