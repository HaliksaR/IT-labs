function srcPath(index) {
    return 'src/dg' + index + '.gif';
}

setInterval(start, 500);

function start () {
    let now     = new Date(),
        seconds = now.getSeconds().toString(),
        minutes = now.getMinutes().toString(),
        hour    = now.getHours().toString(),
        day     = now.getDate().toString(),
        month   = (now.getMonth() + 1).toString(),
        year    = now.getFullYear().toString();
    time(seconds, minutes, hour, day, month, year);
}

function time(seconds, minutes, hour, day, month, year) {
    for (let i = 0; i < arguments.length; i++) {
        if (arguments[i].length < 2) {
            arguments[i] = '0' + arguments[i];
        }
    }
    for (let i = 0; i < 2; i++) {
        document.getElementById('s' + (i + 1)).src      = srcPath(seconds.charAt(i));
        document.getElementById('m' + (i + 1)).src      = srcPath(minutes.charAt(i));
        document.getElementById('h' + (i + 1)).src      = srcPath(hour.charAt(i));
        document.getElementById('day' + (i + 1)).src    = srcPath(day.charAt(i));
        document.getElementById('month' + (i + 1)).src  = srcPath(month.charAt(i));
    }
    for (let i = 0; i < 4; i++) {
        document.getElementById('y' + (i + 1)).src      = srcPath(year.charAt(i));
    }
}
